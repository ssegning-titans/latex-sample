# Latex Sample

This is a sample of a latex document.

0. Ensure you have docker installed on your machine. it simpler.
   You can choose to use the cli version, but I cannot
   ensure this would work the same way. I'll assume a variable `IMAGE_NAME` exist and is set to `latex-sample-project`.
   Also, another variable `OUT_DIR` is set to `out`. The last one is `PWD` which is set to the current working
   directory.

   To do that, you can do the following:
   ```bash
   IMAGE_NAME=my-latex-project
   PWD=$(pwd)
   OUT_DIR=$PWD/out
   ```

    1. Ensure the out dir does exist.
        ```bash
        mkdir -p ${OUT_DIR}
        ```

    2. Build the docker image
         ```bash
         docker build -t ${IMAGE_NAME} .
         ```

## How to use

These are the commands you can use to run the latex document. There are to be run from the root of the project.
Also, inside a docker container. Basically, you need to run the following command like this:

```bash
docker run --rm -v ${PWD}/src:/workspace -v ${OUT_DIR}:/workspace/out ${IMAGE_NAME} sh -c "<<command>>"
```

1. To run and build the latex document, run the following command:
   ```bash
   latexmk -pdf -outdir=/workspace/out main.tex
   ```

2. To clean the latex build, run the following command:
   ```bash
   latexmk -c -outdir=/workspace/out main.tex
   ```

3. To clean the latex build and remove the output directory, run the following command:
   ```bash
   latexmk -C -outdir=/workspace/out main.tex
   ```

4. To run the latex document in watch mode, run the following command:
   ```bash
   latexmk -pdf -outdir=/workspace/out -pvc main.tex
   ```

5. To run the latex document in watch mode and remove the output directory, run the following command:
   ```bash
   latexmk -pdf -outdir=/workspace/out -pvc -C main.tex
   ```
   

# CI/CD
Please look into the `.gitlab-ci.yml` file for more information on how to use this in a CI/CD pipeline.