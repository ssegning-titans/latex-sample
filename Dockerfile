# Use a base image with LaTeX installed
FROM aergus/latex

# Install additional LaTeX packages if needed
RUN apt-get update && \
    apt-get install -y \
    latexmk

# Set the working directory
WORKDIR /workspace
